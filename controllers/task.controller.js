const { saveTaskResult } = require('../services/task.service');

exports.task2 = async (req, res) => {
    try {
        let n = req.body.number;
        n = parseInt(n);
        if (n <= 0) {
            return res.status(400).json({
                result: 'Дане число не є натуральним'
            });
        }
        n = n.toString()
        let split_n = n.split('');
        let arr = split_n.filter((num) => num == 1)
        let result = arr.length

        await saveTaskResult(2, result);

        res.json({
            result: result
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: 'Internal server error' });
    }
};
