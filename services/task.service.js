const TaskResult = require('../models/calculation');

async function saveTaskResult(taskId, result) {
    const taskResult = new TaskResult({ taskId, result });
    return await taskResult.save();
}

module.exports = {
    saveTaskResult,
};
