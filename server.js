const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const app = express();
const { port, mongodb_uri } = require('./config');
const taskRouter = require('./routes/task.route');

mongoose.connect(mongodb_uri)
  .then(() => {
    console.log('Mongo DB connected');
  });

const PORT = process.env.PORT || 3018;

app.use(express.json());



app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});