exports.checkInput = (req, res, next) => {
  const { number } = req.body;

  if (typeof number !== 'number' || number <= 0) {
    return res.status(400).json({ error: 'Число повинно бути натуральним' });
  }

  next();
};