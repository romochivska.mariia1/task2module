const mongoose = require('mongoose');

const taskResultSchema = new mongoose.Schema({
  taskId: {
    type: Number,
    required: true
  },
  result: {
    type: Number,
    required: true
  },
  timestamp: {
    type: Date,
    default: Date.now
  }
});

const TaskResult = mongoose.model('TaskResult', taskResultSchema);

module.exports = TaskResult;
