const express = require('express');
const router = express.Router();
const taskController = require('../controllers/task.controller');


router.post('/task2', taskController.task2);

module.exports = router;
